public class HowitzerShooting {
    //Стрельба из гаубицы. Дан угол возвышения ствола а и начальная скорость полёта снаряда. v км/ч. Вычислить расстояние полёта снаряда. Реализовать решения для угла в градусах и в радианах.
    public static void main(String args[]){
        System.out.println(gun(10,30));
    }
    public static double gun(double speed, double alpha){
        return ((speed * speed) / 10) * Math.sin(Math.toRadians(2 * alpha));
    }
}
